package soft.app.android_test_task

import android.content.Context
import android.os.Bundle
import android.widget.TextView
import androidx.activity.viewModels
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.floatingactionbutton.FloatingActionButton
import soft.app.android_test_task.fragment_item_send_push.SharedKeys
import soft.app.android_test_task.page_activity.ViewModelPageActivity

class PageActivity : FragmentActivity() {
// ViewPager2
    private lateinit var viewPager2: ViewPager2
// viewModel
    private val viewModelPageActivity: ViewModelPageActivity by viewModels()
// buttons
    private lateinit var buttonAdd: FloatingActionButton
    private lateinit var buttonRemove: FloatingActionButton
// textView
    private lateinit var numberPage: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_slide)
// ViewPager2
        viewPager2 = findViewById(R.id.view_pager_2)
// buttons
        buttonAdd = findViewById(R.id.add_button)
        buttonRemove = findViewById(R.id.remove_button)
// textView
        numberPage = findViewById(R.id.number_fragment)
// sharedPreferences
        val share = this.getSharedPreferences("SHARED", Context.MODE_PRIVATE)

        var quantityFragment = share.getInt(SharedKeys.QuantityFragment.name, 1)
        var positionUser = share.getInt(SharedKeys.PositionUser.name, 0)

        viewModelPageActivity.userPosition.observe(this as LifecycleOwner) {
                positionUser = it
        }
        viewModelPageActivity.quantityFragment.observe(this as LifecycleOwner){
            quantityFragment = it
        }

// button for add new fragment
        buttonAdd.setOnClickListener {
            quantityFragment++
            share.edit()
                .putInt(SharedKeys.QuantityFragment.name, quantityFragment)
                .apply()

            viewModelPageActivity.updatePager(  pager = viewPager2,
                fragmentActivity = this,
                qualityPage = quantityFragment,
                numberPage = numberPage,
                viewPager2 = viewPager2,
                buttonRemove = buttonRemove,
                shared = share)

            viewPager2.setCurrentItem(positionUser, false)
        }

// button for delete old fragment and his notifications
        buttonRemove.setOnClickListener {
            if (quantityFragment > 1){
                quantityFragment--
                share.edit()
                    .putInt(SharedKeys.QuantityFragment.name, quantityFragment)
                    .apply()
            }

            viewModelPageActivity.updatePager(  pager = viewPager2,
                fragmentActivity = this,
                qualityPage = quantityFragment,
                numberPage = numberPage,
                viewPager2 = viewPager2,
                buttonRemove = buttonRemove,
                shared = share)

            viewModelPageActivity.clearNotification(quantityFragment, this)

            viewPager2.setCurrentItem(positionUser, false)
        }

        viewModelPageActivity.updatePager( pager = viewPager2,
            fragmentActivity = this,
            qualityPage = quantityFragment,
            numberPage = numberPage,
            viewPager2 = viewPager2,
            buttonRemove = buttonRemove,
            shared = share)

        viewModelPageActivity.moveByClickingNotification( intent = intent,
                                                          viewPager2 = viewPager2,
                                                          positionUser = positionUser)
    }

    override fun onBackPressed() {
        if (viewPager2.currentItem == 0) {
            super.onBackPressed()
        } else {
            viewPager2.currentItem = viewPager2.currentItem - 1
        }
    }
}










