package soft.app.android_test_task.page_activity

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.View
import android.widget.TextView
import androidx.core.app.NotificationCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.floatingactionbutton.FloatingActionButton
import soft.app.android_test_task.fragment_item_send_push.SendPushAdapter
import soft.app.android_test_task.fragment_item_send_push.SharedKeys

class ViewModelPageActivity: ViewModel() {

    val quantityFragment: MutableLiveData<Int> by lazy{
        MutableLiveData<Int>()
    }
    val userPosition: MutableLiveData<Int> by lazy{
        MutableLiveData<Int>()
    }

// for update View Pager
    fun updatePager(pager: ViewPager2, fragmentActivity: FragmentActivity, qualityPage: Int,
                    numberPage: TextView, viewPager2: ViewPager2, buttonRemove: FloatingActionButton,
                    shared: SharedPreferences
    ){
        pager.adapter = SendPushAdapter(fragmentActivity, qualityPage, numberPage,
            viewPager2, buttonRemove, shared, this)
    }

// function for control work SendPushAdapter
    fun controlPagerAdapter(numberPage: TextView, buttonRemove: FloatingActionButton,
                            shared: SharedPreferences, viewPager2: ViewPager2, quantityFragments: Int){

        val pos = viewPager2.currentItem + 1

        numberPage.text = pos.toString()

        when(pos){
            1 -> buttonRemove.visibility = View.GONE
            else -> buttonRemove.visibility = View.VISIBLE
        }

        shared.edit()
            .putInt(SharedKeys.PositionUser.name, viewPager2.currentItem)
            .apply()

        this.userPosition.value = viewPager2.currentItem

        this.quantityFragment.value = quantityFragments
    }

// for delete push notification by index
    fun clearNotification(notificationId: Int, activity: FragmentActivity?){
        val notificationManager =
            activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        notificationManager?.cancel(notificationId)
    }
// when click on notifications, it moves the user to the fragment
    fun moveByClickingNotification(intent: Intent, viewPager2: ViewPager2, positionUser: Int){
        val extras = intent.getIntExtra("fragmentPosition", 0)

    val title = intent.data?.getQueryParameter("ContentTitle")
    println("title == ${intent}")

        if (extras != 0){
            this.userPosition.value = extras
            viewPager2.setCurrentItem(extras, false)
            println("extras == $extras")
        }else{
            viewPager2.setCurrentItem(positionUser, false)
        }
    }

}