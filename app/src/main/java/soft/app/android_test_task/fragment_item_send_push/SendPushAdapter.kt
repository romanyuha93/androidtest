package soft.app.android_test_task.fragment_item_send_push

import android.content.SharedPreferences
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.floatingactionbutton.FloatingActionButton
import soft.app.android_test_task.page_activity.ViewModelPageActivity

class SendPushAdapter(fragmentActivity: FragmentActivity,
                      private val quantityFragments: Int,
                      private val numberPage: TextView,
                      private val viewPager2: ViewPager2,
                      private val buttonRemove: FloatingActionButton,
                      private val shared: SharedPreferences,
                      private val viewModelPageActivity: ViewModelPageActivity
)
    : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int = quantityFragments

    override fun createFragment(position: Int): Fragment = FragmentCreatePush(position)

    override fun getItemId(position: Int): Long {

        viewModelPageActivity.controlPagerAdapter(numberPage = numberPage,
            buttonRemove = buttonRemove,
            shared = shared,
            viewPager2 = viewPager2,
            quantityFragments = quantityFragments)

        return super.getItemId(position)
    }
}