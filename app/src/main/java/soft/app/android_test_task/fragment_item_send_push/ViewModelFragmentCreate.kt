package soft.app.android_test_task.fragment_item_send_push

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import soft.app.android_test_task.PageActivity
import soft.app.android_test_task.R

class ViewModelFragmentCreate: ViewModel() {

// create notification channel for different versions of the system
    fun createNotificationChannel(CHANNEL_ID: String, fragment: Fragment,
                                  activity: FragmentActivity?) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val descriptionText = fragment.getString(R.string.create_new_notification)
            val channel = NotificationChannel(
                CHANNEL_ID,
                "Messenger",
                NotificationManager.IMPORTANCE_HIGH).apply {
                description = descriptionText
            }

            val notificationManager: NotificationManager =
                activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
// get pending intent for create motion in push notification
    @SuppressLint("UnspecifiedImmutableFlag")
    private fun getPendingIntent(context: Context?, position: Int): PendingIntent {
        val intent = Intent(context, PageActivity::class.java)

        intent.putExtra("fragmentPosition", position)

    return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
}
// build and get ready notification
    fun getNotification(context: Context?, CHANNEL_ID: String, position: Int,
                                resources: Resources) : Notification?{
        val builder = context?.let { _ ->
            NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_baseline_article_24)
                .setContentTitle("Chat heads active")
                .setContentText("Notification ${position + 1}")
                .setColor(ContextCompat.getColor(context, R.color.blue))
                .setLargeIcon(
                    BitmapFactory.decodeResource(resources,
                    R.drawable.notification_icon
                ))
                .setStyle(
                    NotificationCompat.BigPictureStyle()
                    .bigPicture(
                        BitmapFactory.decodeResource(resources,
                        R.drawable.notification_icon
                    ))
                    .bigLargeIcon(null))

                .setContentIntent(getPendingIntent(context = context, position = position))
                .build()
        }

        return builder
    }

}