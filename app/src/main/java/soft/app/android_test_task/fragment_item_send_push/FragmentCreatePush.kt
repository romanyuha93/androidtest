package soft.app.android_test_task.fragment_item_send_push

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import soft.app.android_test_task.R

private const val CHANNEL_ID = "my_channel"

class FragmentCreatePush(private val position: Int) : Fragment() {
// button
    private lateinit var pushButton: Button
// viewModel
    private val viewModelFragmentCreate: ViewModelFragmentCreate by activityViewModels()

    @SuppressLint("RemoteViewLayout", "UnspecifiedImmutableFlag")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.view_item_fragment, container, false)
// button
        pushButton = view.findViewById(R.id.push_button)

// button for create push notification
        pushButton.setOnClickListener {

            viewModelFragmentCreate.createNotificationChannel( CHANNEL_ID = CHANNEL_ID,
                fragment = this,
                activity = activity)

            with(context?.let { context -> NotificationManagerCompat.from(context) }){
                viewModelFragmentCreate.getNotification( context = context,
                    CHANNEL_ID = CHANNEL_ID,
                    position = position,
                    resources = resources)?.let { build ->

                    this?.notify(position, build)
                }
            }
        }

        return view
    }

    override fun onStart() {
        super.onStart()
        requireActivity().window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.teal)
    }

}




