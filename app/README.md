App Android_Test_Task 

version "1.0"

Description
   The application have 3 different buttons:
     1) “+” this button create a new fragment with max page the number +1
        For example, clicking on "+" on page 1 create a page with the number 2.
        clicking again create a page with the number 3 and so on.
     2) “Create new notification” - if clicking on this button will create a notification 
        with the text "notification and the number of pages".
        For example, if you click it on a page with number 10 it create a notification 
        with the text "Notification 10". 
     3) “-” delete button delete the last fragment and all notifications which were 
        created from that fragment.
        User can able to navigate between all fragments.

Failure
     Failed to implement the correct transition to the fragment when clicking on push notifications.
     When you click on any notification, the last fragment from which the notification was sent is displayed
